
********************
Vertex Paint Brushes
********************

Brushes for Grease Pencil *Vertex Paint* mode bundled in the *Essentials* library.

Paint Point Color
   Paints a specified color over the object.

Blur Point Color
   Smooths out the colors of adjacent vertices. In this mode the Color
   Value is ignored. The strength defines how much the colors are blurred.

Average Point Color
   Smooths color by painting the average resulting color from all colors under the brush.

Smear Point Color
   Smudges colors by grabbing the colors under the brush and "dragging" them.
   This can be imagined as a finger painting tool.

Replace Point Color
   Change the color only to the stroke points that already have a color applied.


###########
  Normals
###########

.. toctree::
   :maxdepth: 1

   normal_edit.rst
   weighted_normal.rst

-----

.. toctree::
   :maxdepth: 1

   smooth_by_angle.rst

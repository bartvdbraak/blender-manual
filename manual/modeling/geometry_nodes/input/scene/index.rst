
##########################
  Input Scene Data Nodes
##########################

.. toctree::
   :maxdepth: 1

   3d_cursor.rst
   active_camera.rst
   collection_info.rst
   image_info.rst
   is_viewport.rst
   mouse_position.rst
   object_info.rst
   scene_time.rst
   self_object.rst
   viewport_transform.rst

.. index:: Geometry Nodes; Separate Transform
.. _bpy.types.FunctionNodeSeparateTransform:

***********************
Separate Transform Node
***********************

.. figure:: /images/node-types_FunctionNodeSeparateTransform.webp
   :align: right
   :alt: Separate Transform node.

The *Separate Transform* node separates a :term:`Transformation Matrix`
into a translation vector, a rotation vector, and a scale vector.


Inputs
======

Transform
   The transformation matrix to separate.


Properties
==========

This node has no properties.


Outputs
=======

Translation
   The translation vector.
Rotation
   The rotation vector.
Scale
   The scale vector.

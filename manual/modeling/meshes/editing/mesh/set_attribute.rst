.. _bpy.ops.mesh.attribute_set:

*************
Set Attribute
*************

.. reference::

   :Mode:      Edit Mode
   :Menu:      :menuselection:`Mesh --> Set Attribute`

Sets the value of the :term:`Active` :term:`Attribute` for the selected element.
"Active attribute" here means the attribute that is currently selected in the
:ref:`Attributes list <bpy.types.AttributeGroup>`.

When the operator is executed a pop-up window will display the attribute's name and current value.
From here, the value field came be adjusted to change the attribute's value.

.. seealso::

   Attribute values can be viewed in the :doc:`/editors/spreadsheet` editor.

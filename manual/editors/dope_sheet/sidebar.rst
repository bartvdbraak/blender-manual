
*******
Sidebar
*******

Action Panel
============

.. figure:: /images/animation_actions_range.png
   :align: center

   Actions with and without a Manual Frame Range in Dope Sheet.

When the editor is in :doc:`Action Editor </editors/dope_sheet/modes/action>` mode,
or in Dope Sheet mode with a channel selected that belongs to an action,
this panel allows changing some settings of that action.
See :ref:`Action Properties <actions-properties>` for details.


Custom Properties
=================

Create and manage your own properties to store data in the action's data block.
See the :ref:`Custom Properties <files-data_blocks-custom-properties>` page for more information.

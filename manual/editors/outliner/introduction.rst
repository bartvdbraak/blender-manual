.. index:: Editors; Outliner

************
Introduction
************

.. figure:: /images/editors_outliner_introduction_interface.png
   :align: right

   The Outliner editor.

The *Outliner* shows the content of the blend-file in a tree. You can use it to:

- Get an overview of the data in the scene.
- Select and deselect objects.
- Make objects unselectable or invisible in the 3D Viewport.
- Exclude objects from rendering.
- Duplicate objects.
- Delete objects.
- Manage parent/child relationships and :doc:`collections </scene_layout/collections/introduction>`.

Items with an arrow on the left can be expanded. Click it with :kbd:`LMB` to expand a single item,
drag :kbd:`LMB` to expand multiple items, or click :kbd:`Shift-LMB` to expand an item recursively.


Example
=======

.. figure:: /images/editors_outliner_introduction_example.png

   The Outliner with different kinds of data.

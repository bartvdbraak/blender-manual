
#######
  Mix
#######

.. toctree::
   :maxdepth: 1

   alpha_over.rst

----------

.. toctree::
   :maxdepth: 1

   combine_color.rst
   separate_color.rst

----------

.. toctree::
   :maxdepth: 1

   mix_color.rst
   z_combine.rst

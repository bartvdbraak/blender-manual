
****************
Flatten/Contrast
****************

.. reference::

   :Mode:      Sculpt Mode
   :Brush:     :menuselection:`Asset Shelf --> Flatten/Contrast`

Flatten or contrast surfaces by pulling and pushing them towards
(or away from) a defined medium height.
This medium height is determined via an area plane within the brush radius.

This area plane height can be further defined in the brush settings.


Brush Settings
==============

General
-------

Direction :kbd:`Ctrl`
   Invert the direction to push surfaces away from the
   :ref:`sculpt plane <bpy.types.Brush.sculpt_plane>`,
   creating more surface contrast as a result.

.. note::

   More info at :ref:`sculpt-tool-settings-brush-settings-general` brush settings
   and on :ref:`sculpt-tool-settings-brush-settings-advanced` brush settings.


***********
Scrape/Fill
***********

.. reference::

   :Mode:      Sculpt Mode
   :Brush:     :menuselection:`Asset Shelf --> Scrape/Fill`
   :Shortcut:  :kbd:`Shift-T`

Similar to the :doc:`Flatten </sculpt_paint/sculpting/brushes/flatten>` brush,
but only pushes surfaces downward to the medium height.

Although :kbd:`Ctrl` can be held to invert the effect to a Fill brush,
if *Invert to Fill* is enabled.
When disabled, the inverted direction will push surfaces away.


Brush Settings
==============

General
-------

.. note::

   More info at :ref:`sculpt-tool-settings-brush-settings-general` brush settings
   and on :ref:`sculpt-tool-settings-brush-settings-advanced` brush settings.


Unique
------

Invert to Fill
   When enabled, holding :kbd:`Ctrl` while sculpting
   changes the brush behavior to be the same as the *Fill* brush.
   When disabled, holding :kbd:`Ctrl` while sculpting,
   will push vertices above the cursor up away from the cursor.

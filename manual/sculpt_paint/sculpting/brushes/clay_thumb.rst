
**********
Clay Thumb
**********

.. reference::

   :Mode:      Sculpt Mode
   :Brush:     :menuselection:`Asset Shelf --> Clay Thumb`

Similar to the :doc:`/sculpt_paint/sculpting/brushes/clay` brush.
It imitates the effect of deforming clay with the finger, accumulating material during the stroke.
The sculpt plane tilts during the stroke in the front part of the brush to achieve this effect.


Brush Settings
==============

.. note::

   More info at :ref:`sculpt-tool-settings-brush-settings-general` brush settings
   and on :ref:`sculpt-tool-settings-brush-settings-advanced` brush settings.

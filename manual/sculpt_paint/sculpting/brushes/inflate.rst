
***************
Inflate/Deflate
***************

.. reference::

   :Mode:      Sculpt Mode
   :Brush:     :menuselection:`Asset Shelf --> Inflate/Deflate`
   :Shortcut:  :kbd:`I`

Similar to :doc:`Draw </sculpt_paint/sculpting/brushes/clay>`,
except that vertices are moved in the direction of their own normals.
Especially useful when sculpting meshes with a lot of curvature.

Also available as a :doc:`Mesh Filter </sculpt_paint/sculpting/tools/mesh_filter>`
to inflate all unmasked areas at once.


Brush Settings
==============

General
-------

Direction
   Either Inflate or Deflate sculpted areas.
   This is different from the typical Add & Subtract.

.. note::

   More info at :ref:`sculpt-tool-settings-brush-settings-general` brush settings
   and on :ref:`sculpt-tool-settings-brush-settings-advanced` brush settings.

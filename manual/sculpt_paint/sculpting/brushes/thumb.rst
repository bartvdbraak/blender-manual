
*****
Thumb
*****

.. reference::

   :Mode:      Sculpt Mode
   :Brush:     :menuselection:`Asset Shelf --> Thumb`

Similar to the :doc:`Grab </sculpt_paint/sculpting/brushes/grab>` brush,
but instead only moves the geometry along the surface (using the area plane).
This is useful for grabbing surfaces with a very specific direction,
or without making too impactful changes to the overall object shape.


Brush Settings
==============

General
-------

.. note::

   More info at :ref:`sculpt-tool-settings-brush-settings-general` brush settings
   and on :ref:`sculpt-tool-settings-brush-settings-advanced` brush settings.


*****
Nudge
*****

.. reference::

   :Mode:      Sculpt Mode
   :Brush:     :menuselection:`Asset Shelf --> Nudge`

Moves vertices in the direction of the brush stroke.

Similar to the :doc:`Snake Hook </sculpt_paint/sculpting/brushes/snake_hook>` brush,
but instead only moves the geometry along the surface (using the area plane).
This is useful for grabbing geometry along curved surfaces,
without making too impactful changes to the overall object shape.


Brush Settings
==============

General
-------

.. note::

   More info at :ref:`sculpt-tool-settings-brush-settings-general` brush settings
   and on :ref:`sculpt-tool-settings-brush-settings-advanced` brush settings.

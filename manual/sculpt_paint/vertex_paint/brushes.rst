
*******
Brushes
*******


Brush Types
===========

See :ref:`Brush Type <sculpt-tool-settings-brush-type>`.

Available brush types are listed here, together with brushes from the
*Essentials* asset library using them.

Paint Vertex
   Brushes: Paint Vertex

   Paints a specified color over the object.

Blur
   Brushes: Blur Vertex

   Smooths out the colors of adjacent vertices. In this mode the Color
   Value is ignored. The strength defines how much the colors are blurred.

Average
   Brushes: Average Vertex

   Smooths color by painting the average resulting color from all colors under the brush.

Smear
   Brushes: Smear Vertex

   Smudges colors by grabbing the colors under the brush and "dragging" them.
   This can be imagined as a finger painting tool.

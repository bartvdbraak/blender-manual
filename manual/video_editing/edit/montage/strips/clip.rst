.. _bpy.types.MovieClipSequence:

**********
Clip Strip
**********

Clip can be modified within the :doc:`/editors/clip/index`.


Options
=======

This strip has no options.

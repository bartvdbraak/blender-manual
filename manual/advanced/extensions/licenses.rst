.. _extensions-licenses:
.. index:: Licenses

******************
Extension Licenses
******************

For add-ons and themes the recommended license is
`GNU General Public License v2.0 or later <https://spdx.org/licenses/GPL-2.0-or-later.html>`__.
For assets, the required license is `Public Domain (CC0) <https://spdx.org/licenses/CC0-1.0.html>`__.

The `Blender Extensions Platform <https://extensions.blender.org>`__ only supports
free and open source extensions compatible with Blender's license:
`GNU General Public License v3.0 or later <https://spdx.org/licenses/GPL-3.0-or-later.html>`__.

This allows extensions to be packed with Blender and distributed in compliance
with the governing principles of the `Blender license <https://www.blender.org/about/license/>`__.

More GPL and LGPL Licenses
==========================

Some third-party add-on libraries may require a different compatible license.

In those cases a few variant versions of GNU GPL are also accepted:

.. list-table::
   :header-rows: 1

   * - Identifier
     - License
   * - SPDX:GPL-2.0-or-later
     - `GNU General Public License v2.0 or later <https://spdx.org/licenses/GPL-2.0-or-later.html>`__
   * - SPDX:GPL-3.0-or-later
     - `GNU General Public License v3.0 or later <https://spdx.org/licenses/GPL-3.0-or-later.html>`__
   * - SPDX:LGPL-2.1-or-later
     - `GNU Lesser General Public License v2.1 or later <https://spdx.org/licenses/LGPL-2.1-or-later.html>`__
   * - SPDX:LGPL-3.0-or-later
     - `GNU Lesser General Public License v3.0 or later <https://spdx.org/licenses/LGPL-3.0-or-later.html>`__

More Compatible Licenses
========================

In some exceptional cases other licenses may be required. Extensions are still accepted under these licenses:

.. list-table::
   :header-rows: 1

   * - Identifier
     - License
   * - SPDX:BSD-1-Clause
     - `BSD 1-Clause "Simplified" License <https://spdx.org/licenses/BSD-1-Clause.html>`__
   * - SPDX:BSD-2-Clause
     - `BSD 2-Clause "Simplified" License <https://spdx.org/licenses/BSD-2-Clause.html>`__
   * - SPDX:BSD-3-Clause
     - `BSD 3-Clause "New" or "Revised" License <https://spdx.org/licenses/BSD-3-Clause.html>`__
   * - SPDX:BSL-1.0
     - `Boost Software License 1.0 <https://spdx.org/licenses/BSL-1.0.html>`__
   * - SPDX:MIT
     - `MIT License <https://spdx.org/licenses/MIT.html>`__
   * - SPDX:MIT-0
     - `MIT No Attribution <https://spdx.org/licenses/MIT-0.html>`__
   * - SPDX:MPL-2.0
     - `Mozilla Public License 2.0 <https://spdx.org/licenses/MPL-2.0.html>`__
   * - SPDX:Pixar
     - `Pixar License <https://spdx.org/licenses/Pixar.html>`__
   * - SPDX:Zlib
     - `Zlib License <https://spdx.org/licenses/Zlib.html>`__
